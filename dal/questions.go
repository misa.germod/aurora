package dal

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

type QuestionsRepository struct {
	session *pgx.Conn
}

func NewQuestionsRepository(session *pgx.Conn) *QuestionsRepository {
	return &QuestionsRepository{session: session}
}

func (r *QuestionsRepository) Add(ctx context.Context, question *Question) (*Question, error) {
	queryStmt := `insert into questions (id, right_position, question, variants) VALUES ($1,$2,$3,$4)`
	question.ID = GetRandomUUID()
	_, err := r.session.Exec(
		ctx,
		queryStmt,
		question.ID,
		question.RightPosition,
		question.Question,
		question.Variants,
	)

	return question, err
}

func (r *QuestionsRepository) GetQuestionByID(ctx context.Context, id uuid.UUID) (*Question, error) {
	var (
		queryStmt = `select id, right_position, variants, question from questions where id = $1`
		question  = new(Question)
	)

	err := r.session.QueryRow(ctx, queryStmt, id).Scan(
		&question.ID,
		&question.RightPosition,
		&question.Variants,
		&question.Question,
	)

	return question, err
}

func (r *QuestionsRepository) GetRandomQuestionIDs(ctx context.Context, count int) ([]uuid.UUID, error) {
	if count <= 0 {
		return nil, errors.New("invalid input value")
	}

	var (
		queryStmt   = `select id from questions order by random() limit $1`
		questionIDs = make([]uuid.UUID, 0, count)
	)

	scanner, err := r.session.Query(ctx, queryStmt, count)
	if err != nil {
		return nil, err
	}
	for scanner.Next() {
		id := new(uuid.UUID)
		err := scanner.Scan(id)
		if err != nil {
			return nil, err
		}
		questionIDs = append(questionIDs, *id)
	}
	return questionIDs, nil
}
