package dal

import (
	"context"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

type SecretsRepository struct {
	session *pgx.Conn
}

func NewSecretsRepository(session *pgx.Conn) *SecretsRepository {
	return &SecretsRepository{session: session}
}

func (r *SecretsRepository) Add(ctx context.Context, s *Secret) (*Secret, error) {
	queryStmt := `insert into secrets (id, allowed_fails, current_fails, created_time_utc, expiration_time_utc, deadline_for_answer_utc, question_position, questions, secret)
		VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)`

	s.ID = GetRandomUUID()
	s.CreatedTimeUTC = time.Now().UTC()
	s.DeadlineForAnswerUTC = s.ExpirationTimeUTC

	_, err := r.session.Exec(
		ctx,
		queryStmt,
		s.ID,
		s.AllowedFails,
		s.CurrentFails,
		s.CreatedTimeUTC,
		s.ExpirationTimeUTC,
		s.DeadlineForAnswerUTC,
		s.QuestionPosition,
		s.Questions,
		s.Secret,
	)

	return s, err
}

func (r *SecretsRepository) GetSecretByID(ctx context.Context, id uuid.UUID) (*Secret, error) {
	var (
		queryStmt = `select id, allowed_fails, current_fails, created_time_utc, expiration_time_utc, deadline_for_answer_utc, question_position, questions, secret from secrets 
				where id = $1`
		secret = new(Secret)
	)

	err := r.session.QueryRow(ctx, queryStmt, id).Scan(
		&secret.ID,
		&secret.AllowedFails,
		&secret.CurrentFails,
		&secret.CreatedTimeUTC,
		&secret.ExpirationTimeUTC,
		&secret.DeadlineForAnswerUTC,
		&secret.QuestionPosition,
		&secret.Questions,
		&secret.Secret)

	return secret, err
}

func (r *SecretsRepository) DeleteSecretByID(ctx context.Context, id uuid.UUID) error {
	var (
		queryStmt = `delete from secrets where id = $1`
	)
	_, err := r.session.Exec(ctx, queryStmt, id)
	return err
}

func (r *SecretsRepository) SetCurrentFails(ctx context.Context, id uuid.UUID, currentFails int) error {
	var (
		queryStmt = `update secrets set current_fails = $1 where id = $2`
	)

	_, err := r.session.Exec(ctx, queryStmt, currentFails, id)
	return err

}

func (r *SecretsRepository) SetQuestionPosition(ctx context.Context, id uuid.UUID, questionPosition int) error {
	var (
		queryStmt = `update secrets set question_position = $1 where id = $2`
	)

	_, err := r.session.Exec(ctx, queryStmt, questionPosition, id)
	return err

}

func (r *SecretsRepository) SetDeadlineForAnswerUTC(ctx context.Context, id uuid.UUID, deadline time.Time) error {
	var (
		queryStmt = `update secrets set deadline_for_answer_utc = $1 where id = $2`
	)
	_, err := r.session.Exec(ctx, queryStmt, deadline, id)
	return err
}
