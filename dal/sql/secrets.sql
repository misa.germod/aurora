create table if not exists secrets
(
    id uuid primary key,
    current_fails int,
    allowed_fails int,
    created_time_utc timestamp,
    expiration_time_utc timestamp,
    deadline_for_answer_utc timestamp,
    question_position int,
    questions uuid[],
    secret text
)