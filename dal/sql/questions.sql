create table if not exists questions
(
    id uuid primary key,
    right_position int,
    question text,
    variants text[]
)