package dal

import (
	"github.com/google/uuid"
	"time"
)

type Question struct {
	ID            uuid.UUID
	RightPosition int
	Question      string
	Variants      []string
}

type Secret struct {
	ID                   uuid.UUID
	AllowedFails         int
	CurrentFails         int
	QuestionPosition     int
	Questions            []uuid.UUID
	Secret               string
	CreatedTimeUTC       time.Time
	DeadlineForAnswerUTC time.Time
	ExpirationTimeUTC    time.Time
}

func GetRandomUUID() uuid.UUID {
	uuid, _ := uuid.NewUUID()
	return uuid
}
