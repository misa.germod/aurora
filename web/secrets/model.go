package secrets

type NewSecret struct {
	AllowedFails       int    `json:"allowed_fails"`
	QuestionsCount     int    `json:"questions_count"`
	ExpirationTimeUnix int64  `json:"expiration_time_unix"`
	Secret             string `json:"secret"`
}

type QuestionsResponse struct {
	Question           string   `json:"question"`
	Variants           []string `json:"variants"`
	ExpirationTimeUnix int64    `json:"expiration_time_unix"`
	DeadlineTimeUnix   int64    `json:"deadline_time_unix"`
}

type NextActionResponse struct {
	Action string `json:"action"`
	Body   string `json:"body"`
}

type ErrorResponse struct {
	Error       string `json:"error"`
	Description string `json:"description"`
}
