package secrets

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"

	"aurora/dal"
)

type Handler struct {
	questionsRepository *dal.QuestionsRepository
	secretsRepository   *dal.SecretsRepository
}

func NewHandler(
	questionsRepository *dal.QuestionsRepository,
	secretsRepository *dal.SecretsRepository,
) *Handler {
	return &Handler{
		questionsRepository: questionsRepository,
		secretsRepository:   secretsRepository,
	}
}

func (h *Handler) Add(w http.ResponseWriter, r *http.Request) {
	var (
		newSecret = new(NewSecret)
		ctx       = r.Context()
	)

	err := json.NewDecoder(r.Body).Decode(newSecret)
	if err != nil {
		sendJSON(
			w,
			&ErrorResponse{
				Error:       "invalid_body",
				Description: "invalid body",
			},
			http.StatusBadRequest,
		)
		return
	}

	expirationTimeUTC := time.Unix(newSecret.ExpirationTimeUnix, 0).UTC()

	if time.Now().UTC().After(expirationTimeUTC) {
		sendJSON(
			w,
			&ErrorResponse{
				Error:       "invalid_expiration_time",
				Description: "invalid expiration time",
			},
			http.StatusBadRequest,
		)
		return
	}

	questionIDs, err := h.questionsRepository.GetRandomQuestionIDs(ctx, newSecret.QuestionsCount)
	if err != nil {
		w.WriteHeader(http.StatusBadGateway)
		log.Printf("GetRandomQuestionIDs fail, count: %d, err: %s\r\n", newSecret.QuestionsCount, err)
		return
	}

	dbSecret := &dal.Secret{
		AllowedFails:      newSecret.AllowedFails,
		QuestionPosition:  0, // Default for all new secrets
		Questions:         questionIDs,
		Secret:            newSecret.Secret, // TODO: encrypt it!
		ExpirationTimeUTC: expirationTimeUTC,
	}

	dbSecret, err = h.secretsRepository.Add(ctx, dbSecret)
	if err != nil {
		w.WriteHeader(http.StatusBadGateway)
		log.Printf("GetRandomQuestionIDs fail, count: %v, err: %s\r\n", dbSecret, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	_, err = w.Write([]byte(dbSecret.ID.String()))
	if err != nil {
		log.Printf("failed write reponse: %s, err: %s", dbSecret.ID, err)
	}
}

func (h *Handler) GetQuestion(w http.ResponseWriter, r *http.Request) {
	var (
		vars = mux.Vars(r)
		ctx  = r.Context()
	)

	secretId, err := uuid.Parse(vars["secret_id"])
	if err != nil {
		sendJSON(
			w,
			&ErrorResponse{
				Error:       "bad_secret_id",
				Description: "bad secret id",
			},
			http.StatusBadRequest,
		)
		return
	}

	secret, question, err := h.getSecretAndQuestionForExecute(ctx, w, secretId)
	if err != nil {
		log.Println(secretId, err)
		return
	}

	response := &QuestionsResponse{
		Question:           question.Question,
		Variants:           question.Variants,
		ExpirationTimeUnix: secret.ExpirationTimeUTC.Unix(),
		DeadlineTimeUnix:   secret.DeadlineForAnswerUTC.Unix(),
	}
	sendJSON(w, response, http.StatusOK)
}

func (h *Handler) PutAnswer(w http.ResponseWriter, r *http.Request) {
	var (
		vars              = mux.Vars(r)
		answerPositionStr = vars["answer_position"]
		ctx               = r.Context()
	)

	secretId, err := uuid.Parse(vars["secret_id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte("invalid secret id"))
		return
	}

	answerPosition, err := strconv.Atoi(answerPositionStr)
	if err != nil {
		return
	}

	secret, question, err := h.getSecretAndQuestionForExecute(ctx, w, secretId)
	if err != nil {
		log.Println(secretId, err)
		return
	}

	if question.RightPosition != answerPosition || time.Now().UTC().After(secret.DeadlineForAnswerUTC) {
		secret.CurrentFails++
		err = h.secretsRepository.SetCurrentFails(ctx, secretId, secret.CurrentFails)
		if err != nil {
			log.Printf("failed update allowed_fails secret id: %s, err: %s", secretId, err)
		}
		if secret.CurrentFails > secret.AllowedFails {
			err = h.secretsRepository.DeleteSecretByID(ctx, secretId)
			if err != nil {
				log.Printf("failed delete secret, id: %s, err: %s", secretId, err)
			}
			sendJSON(
				w,
				&ErrorResponse{
					Error:       "failed_secret",
					Description: "failed secret",
				},
				http.StatusBadRequest,
			)
			return
		}
	}

	// move to next question
	secret.QuestionPosition++
	if secret.QuestionPosition >= len(secret.Questions) { // all question passed
		err = h.secretsRepository.DeleteSecretByID(ctx, secretId)
		if err != nil {
			log.Printf("failed delete secret, id: %s, err: %s", secretId, err)
		}
		sendJSON(
			w,
			&NextActionResponse{
				Action: "get_secret",
				Body:   secret.Secret, // TODO: decrypt
			},
			http.StatusOK,
		)
		return
	}

	err = h.secretsRepository.SetQuestionPosition(ctx, secretId, secret.QuestionPosition)
	if err != nil {
		log.Printf("failed update question_position secret id: %s, err: %s", secretId, err)
	}
	secret.DeadlineForAnswerUTC = time.Now().UTC().Add(time.Second * 30) // TODO: use value from configuration

	err = h.secretsRepository.SetDeadlineForAnswerUTC(ctx, secretId, secret.DeadlineForAnswerUTC)
	if err != nil {
		log.Printf("failed update deadline secret id: %s, err: %s", secretId, err)
	}

	sendJSON(
		w,
		&NextActionResponse{
			Action: "get_next_question",
			Body:   "",
		},
		http.StatusOK,
	)
}

func (h *Handler) getSecretAndQuestionForExecute(ctx context.Context, w http.ResponseWriter, secretId uuid.UUID) (*dal.Secret, *dal.Question, error) {
	dbSecret, err := h.secretsRepository.GetSecretByID(ctx, secretId)
	if err != nil {
		sendJSON(
			w,
			&ErrorResponse{
				Error:       "secret_not_found",
				Description: "do not have secret",
			},
			http.StatusNotFound,
		)
		return nil, nil, errors.New("do not have secret")
	}
	if time.Now().UTC().After(dbSecret.ExpirationTimeUTC) {
		sendJSON(
			w,
			&ErrorResponse{
				Error:       "expired_secret",
				Description: "expired secret",
			},
			http.StatusBadRequest,
		)
		return nil, nil, errors.New("expired secret")
	}
	if dbSecret.AllowedFails <= 0 {
		sendJSON(
			w,
			&ErrorResponse{
				Error:       "failed_secret",
				Description: "failed secret",
			},
			http.StatusBadRequest,
		)
		return nil, nil, errors.New("failed secret")
	}

	if len(dbSecret.Questions) <= dbSecret.QuestionPosition {
		sendJSON(
			w,
			&ErrorResponse{
				Error:       "failed_secret",
				Description: "failed secret",
			},
			http.StatusBadGateway,
		)
		return nil, nil, fmt.Errorf("question selection fail, secret id: %s", secretId)
	}

	questionId := dbSecret.Questions[dbSecret.QuestionPosition]
	question, err := h.questionsRepository.GetQuestionByID(ctx, questionId)
	if err != nil {
		sendJSON(
			w,
			&ErrorResponse{
				Error:       "failed_secret",
				Description: "failed secret",
			},
			http.StatusBadGateway,
		)
		return nil, nil, fmt.Errorf("GetQuestionByID fail, question id: %s, err: %s", questionId, err)
	}

	return dbSecret, question, nil
}

func sendJSON(response http.ResponseWriter, body interface{}, statusCode int) {
	jsonResponse, err := json.Marshal(body)
	if err != nil {
		log.Printf("failed marshall: %s, err: %s", body, err)
	}

	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(statusCode)
	_, err = response.Write(jsonResponse)
	if err != nil {
		log.Printf("failed write reponse: %s, err: %s", jsonResponse, err)
	}
}
