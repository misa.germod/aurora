package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4"

	"aurora/dal"
	"aurora/web/secrets"
)

var (
	listenAddr string
)

func main() {
	flag.StringVar(&listenAddr, "listenAddr", ":8080", "")
	flag.Parse()

	connectionString := `postgres://postgres:password@localhost:5432/postgres`
	session, err := pgx.Connect(context.Background(), connectionString)
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close(context.Background())

	//generator(session) // use it for generate questions sample

	secretsRepository := dal.NewSecretsRepository(session)
	questionsRepository := dal.NewQuestionsRepository(session)

	secretsHandler := secrets.NewHandler(questionsRepository, secretsRepository)
	router := mux.NewRouter()
	router.HandleFunc("/api/new_secret", secretsHandler.Add).Methods(http.MethodPost)
	router.HandleFunc("/api/get_secret/{secret_id}", secretsHandler.GetQuestion).Methods(http.MethodGet)
	router.HandleFunc("/api/put_secret/{secret_id}/{answer_position}", secretsHandler.PutAnswer).Methods(http.MethodGet)
	router.HandleFunc("/s/{secret_id}", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./web_files/question.html")
	}).Methods(http.MethodGet)

	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./web_files/")))

	router.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Println(r.RequestURI)
			next.ServeHTTP(w, r)
		})
	})

	srv := &http.Server{
		Handler:      router,
		Addr:         listenAddr,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}

func generator(session *pgx.Conn) {
	questionsRepository := dal.NewQuestionsRepository(session)
	ctx := context.Background()
	for j := 0; j < 1000; j++ {
		for i := 0; i < 4; i++ {
			q := &dal.Question{
				RightPosition: i,
				Question:      fmt.Sprintf("#%d_%d", j, i),
				Variants:      []string{"1", "2", "3", "4"},
			}
			q.Variants[i] = q.Variants[i] + "+"
			_, _ = questionsRepository.Add(ctx, q)
		}
		for i := 0; i < 2; i++ {
			q := &dal.Question{
				RightPosition: i,
				Question:      fmt.Sprintf("#%d_%d", j, i),
				Variants:      []string{"1", "2"},
			}
			q.Variants[i] = q.Variants[i] + "+"
			_, _ = questionsRepository.Add(ctx, q)
		}
	}
	log.Fatal()
}
